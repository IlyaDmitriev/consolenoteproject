﻿namespace ConsoleNotes.Services.Interfaces
{
	public interface ICommandService
	{
		void Handle(string command);
	}
}
